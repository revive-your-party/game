#include "Substance.h"

Substance::Substance(std::string &name, std::string &unit, int quantity, int remainingQuantity,
                     std::string &picturePath) {
    this->name = name;
    this->unit = unit;
    this->quantity = quantity;
    this->remainingQuantity = remainingQuantity;
    this->picturePath = picturePath;
}

std::string Substance::getName() const {
    return name;
}

std::string Substance::getUnit() const {
    return unit;
}

int Substance::getQuantity() const {
    return quantity;
}

int Substance::getRemainingQuantity() const {
    return remainingQuantity;
}

std::string Substance::getPicturPath() const {
    return picturePath;
}

bool operator==(Substance const &A, Substance const &B) {
    return A.getName() == B.getName() && A.getUnit() == B.getUnit();
}

bool operator!=(Substance const &A, Substance const &B) {
    return !(A == B);
}
