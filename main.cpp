#include <iostream>
#include <SFML/Graphics.hpp>

int main() {
    sf::RenderWindow window(sf::VideoMode(600, 600), "SFMLApp works!");
    window.setVerticalSyncEnabled(true);

    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);

    sf::Texture texture_buttonBackground;
    if (!texture_buttonBackground.loadFromFile("sprites/button_background.jpg")) {
        std::cout << "Error while loading 'sprites/button_background.jpg'";
    }

    sf::Sprite button_NewGame;
    button_NewGame.setTexture(texture_buttonBackground);
    button_NewGame.setPosition(0, 150);
    button_NewGame.setScale(1, 1);

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        window.draw(shape);
        window.display();

        if (button_NewGame.getGlobalBounds().contains(sf::Mouse::getPosition(window).x,
                                                      sf::Mouse::getPosition(window).y)) {
            std::cout << "Hovered over button!" << std::endl;
        }
    }

    return 0;
}