#ifndef CPP_LESSON_SUBSTANCE_H
#define CPP_LESSON_SUBSTANCE_H

#include <string>

class Substance {

private:
    std::string name;
    std::string unit;
    int quantity;
    int remainingQuantity;
    std::string picturePath;

public:
    Substance(std::string &name, std::string &unit, int quantity, int remainingQuantity, std::string &picturePath);

    std::string getName() const;

    std::string getUnit() const;

    int getQuantity() const;

    int getRemainingQuantity() const;

    std::string getPicturPath() const;
};

bool operator==(Substance const &A, Substance const &B);

bool operator!=(Substance const &A, Substance const &B);

#endif
