#include "Player.h"

const std::string &Player::getPseudo() const {
    return pseudo;
}

int Player::getScore() const {
    return score;
}

const std::string &Player::getPicturePath() const {
    return picturePath;
}

Player::Player(const std::string &pseudo, const std::string &picturePath) : pseudo(pseudo), picturePath(picturePath) {
    this->pseudo = pseudo;
    this->score = 0;
    this->picturePath = picturePath;
}