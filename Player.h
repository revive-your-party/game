#ifndef CPP_LESSON_PLAYER_H
#define CPP_LESSON_PLAYER_H

#include <string>

class Player {

private:
    std::string pseudo;
    int score;
    std::string picturePath;

public:
    const std::string &getPseudo() const;

    int getScore() const;

    const std::string &getPicturePath() const;

    Player(const std::string &pseudo, const std::string &picturePath);
};


#endif
